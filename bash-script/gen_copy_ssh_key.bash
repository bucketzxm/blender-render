#!/bin/bash

ssh_key_name="ARMCluster"

function ssh_gen_key() {
  yes | ssh-keygen -t rsa -N "" -f "$HOME/.ssh/$ssh_key_name" > /dev/null
  ssh-add "$HOME/.ssh/$ssh_key_name"
}

function ssh_copy_key() {
  local host="$1"
  local port="$2"
  local login="$3"
  local passwd="$4"

  echo "ssh $login@$host:$port"
  cat "$HOME/.ssh/$ssh_key_name.pub" | ssh $login@$host -p $port "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys"
}

ssh_gen_key
ssh_copy_key "192.168.1.101" "22" "ubuntu" "ubuntu"
ssh_copy_key "192.168.1.102" "22" "ubuntu" "ubuntu"
ssh_copy_key "192.168.1.103" "22" "ubuntu" "ubuntu"

