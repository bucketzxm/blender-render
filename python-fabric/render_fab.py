#!/bin/bash/env python

import os
import fnmatch
from fabric.api import *

WORK_DIR = '/home/ubuntu/render_work'
BLENDER_PATH = '$HOME/blender-source/build_linux/bin/'
env.user = 'ubuntu'
env.password = 'ubuntu'
env.hosts = ['192.168.1.101',
             '192.168.1.102',
             '192.168.1.103']
HOST_NUM = len(env.hosts)
master_user = 'simon'
master_host = '192.168.1.2'
cmd_list = []


@parallel
def clean_render_output():
    run('rm -rf /home/ubuntu/render_work/output')


@parallel
def render(filename, start, end):
    try:
        start = int(start)
        end = int(end)
    except ValueError:
        print("ValueError: start/end is not integer (start={}, end={})".format(start, end))

    def gen_render_cmd(filename, start, end, engine='BLENDER_RENDER', output_name='$HOME/render_work/output/render'):
        output_name = '{}_{}_'.format(output_name, start)
        blender_cmd = BLENDER_PATH + 'blender -b {filename} -o {output} -E {ENGINE} -s {start} -e {end} -a'
        blender_cmd = blender_cmd.format(filename=filename, start=start, end=end, ENGINE=engine, output=output_name)
        return blender_cmd

    run('mkdir -p %s' % WORK_DIR)
    host_id = env.hosts.index(env.host)
    frames_per_node = (end - start + 1) / HOST_NUM
    frame_start = host_id * frames_per_node + 1
    frame_end = (host_id + 1) * frames_per_node
    if host_id == HOST_NUM-1:
        frame_end = end
    render_cmd = gen_render_cmd(filename, frame_start, frame_end)
    with cd(WORK_DIR):
        run('mkdir -p output')
        put(filename, os.path.basename(filename))
        run(render_cmd)


@parallel
def fetch_render_output():
    def fetch_each():
        with cd(os.path.join(WORK_DIR, 'output')):
            output_tar = 'output-{}.tar.gz'.format(env.host)
            run('tar czf {} *'.format(output_tar))
            get(output_tar, os.path.join('output', output_tar))

    run('mkdir -p output')
    fetch_each()


def extract():
    tar_files = [f for f in os.listdir('output') if fnmatch.fnmatch(f, '*.tar.gz')]
    with lcd('output'):
        map(lambda f: local('tar xzf ' + f), tar_files)
    local('echo "Extraction Done!"')
