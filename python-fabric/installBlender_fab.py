#!/bin/bash/env python

from fabric.api import *

env.hosts = [
    '192.168.1.101',
    '192.168.1.102',
    '192.168.1.103',
]
env.user = 'ubuntu'
env.password = 'ubuntu'

WORK_DIR = '/home/ubuntu/blender-source/'

@parallel()
def copy_blender_source():
    run('mkdir -p ' + WORK_DIR)
    with cd(WORK_DIR):
        put('./resource/blender-git.tar.gz', './blender-git.tar.gz')
        run('tar -xzf ./blender-git.tar.gz')
        run('rm -f ./blender-git.tar.gz')

@parallel()
def build_blender_source():
    run('mkdir -p ' + WORK_DIR)
    with cd(WORK_DIR):
        sudo('rm -rf $HOME/src')
        # 'blender/build_files/build_environment/install_dep.sh' installs deps into '$HOME/src/' directory,
        # so after extraction, we must soft link 'src' directory to '$HOME/src'
        run('ln -s $HOME/blender-source/src $HOME/src')
        with cd('blender/build_files/build_environment/'):
            sudo('echo "Y" | ./install_deps.sh --skip-ocio --skip-osl')
        with cd('blender'):
            run('make -j4 BUILD_CMAKE_ARGS=" -D WITH_CODEC_SNDFILE=ON -D PYTHON_VERSION=3.5 -D PYTHON_ROOT_DIR=/opt/lib/python-3.5 -D OPENEXR_ROOT_DIR=/opt/lib/openexr -D OPENIMAGEIO_ROOT_DIR=/opt/lib/oiio -D WITH_CYCLES_OSL=OFF -D WITH_LLVM=OFF -D WITH_OPENSUBDIV=ON -D OPENSUBDIV_ROOT_DIR=/opt/lib/osd -D WITH_CODEC_FFMPEG=ON -D FFMPEG_LIBRARIES=\'avformat;avcodec;avutil;avdevice;swscale;rt;theora;theoraenc;theoradec;vorbis;vorbisfile;vorbisenc;ogg;x264;openjpeg\' -D FFMPEG=/opt/lib/ffmpeg"')
        # remove the soft link to cleanup
        run('rm -f $HOME/src')


@parallel()
def clean_apt_lock():
    sudo('rm -f /var/lib/dpkg/lock')
    sudo('rm -f /var/cache/apt/archives/lock')

